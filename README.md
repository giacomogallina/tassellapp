# Tassellib

A gpu rendering experiment compilable to webassembly, deployed [here](https://uz.sns.it/~giacomogallina/tassellazione/index.html).

To compile, install `wasm-pack`

	cargo install wasm-pack

then

	cd tassellib && ./build.sh

after the compilation completes, serve `pkg/`

# Tassellapp

A wrapper to run `tassellib` natively. Just

	cargo run --release
