use std::mem;
use std::borrow::Cow;

use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Window,
};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::wasm_bindgen;
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::JsCast;


async fn run(event_loop: EventLoop<()>, window: Window) {
    // resize of our winit::Window whenever the browser window changes size.
    let window = std::rc::Rc::new(window);
    #[cfg(target_arch = "wasm32")]
    {
        let winit_window = window.clone();
        let closure = wasm_bindgen::closure::Closure::wrap(Box::new(move |e: web_sys::Event| {
            let client_window = web_sys::window().unwrap();
            let size = winit::dpi::LogicalSize::new(
                client_window.inner_width().unwrap().as_f64().unwrap(),
                client_window.inner_height().unwrap().as_f64().unwrap(),
            );
            winit_window.set_inner_size(size);
            winit_window.request_redraw();

        }) as Box<dyn FnMut(_)>);
        web_sys::window().unwrap()
            .add_event_listener_with_callback("resize", closure.as_ref().unchecked_ref())
            .unwrap();
        closure.forget();
    }

    let size = window.inner_size();
    let instance = wgpu::Instance::new(wgpu::Backends::all());
    let surface = unsafe { instance.create_surface(&*window) };
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            force_fallback_adapter: false,
            // Request an adapter which can render to our surface
            compatible_surface: Some(&surface),
        })
    .await
        .expect("Failed to find an appropriate adapter");

    // Create the logical device and command queue
    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::empty(),
                // Make sure we use the texture resolution limits from the adapter, so we can support images the size of the swapchain.
                limits: wgpu::Limits::downlevel_webgl2_defaults()
                    .using_resolution(adapter.limits()),
            },
            None,
            )
        .await
        .expect("Failed to create device");

    // Load the shaders from disk
    let shader = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("shader.wgsl"))),
    });

    let options_bind_group_layout = device.create_bind_group_layout(
        &wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
            ],
            label: Some("options_bind_group_layout"),
        }
        );

    let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[&options_bind_group_layout],
        push_constant_ranges: &[],
    });

    let swapchain_format = surface.get_preferred_format(&adapter).unwrap();

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[swapchain_format.into()],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        //multisample: wgpu::MultisampleState { count: 4, mask: !0, alpha_to_coverage_enabled: false },
        multiview: None,
    });

    let mut config = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: swapchain_format,
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
    };

    surface.configure(&device, &config);

    let options_buffer = device.create_buffer(&wgpu::BufferDescriptor {
        label: Some("options_buffer"),
        size: 16,
        usage: wgpu::BufferUsages::UNIFORM.union(wgpu::BufferUsages::COPY_DST),
        mapped_at_creation: false,
    });

    let mut scale = 3.0;
    let mut origin = 0.0;
    let mut mouse_pressed = false;
    //let mut press_start = None;
    let mut last_mouse_pos = (0.0, 0.0);

    event_loop.run(move |event, _, control_flow| {
        // Have the closure take ownership of the resources.
        // `event_loop.run` never returns, therefore we must do this to ensure
        // the resources are properly cleaned up.
        let _ = (&instance, &adapter, &shader, &pipeline_layout);


        *control_flow = ControlFlow::Wait;
        match event {
            Event::WindowEvent { event, .. } => {
                match event {
                    WindowEvent::Resized(size) => {
                        // Reconfigure the surface with the new size
                        config.width = size.width;
                        config.height = size.height;
                        surface.configure(&device, &config);
                    },
                    WindowEvent::MouseWheel { delta, .. } => {
                        let (dx, dy) = match delta {
                            winit::event::MouseScrollDelta::PixelDelta(p) => (p.x as f64 / 5000.0, p.y as f64 / 5000.0),
                            winit::event::MouseScrollDelta::LineDelta(x, y) => (x as f64 / 100.0, y as f64 / 50.0),
                        };
                        scale *= (-dy).exp();
                        origin += scale * dx;
                        window.request_redraw();
                    },
                    WindowEvent::MouseInput { state, button: winit::event::MouseButton::Left, .. } => {
                        mouse_pressed = state == winit::event::ElementState::Pressed;
                    },
                    WindowEvent::CursorMoved { position: winit::dpi::PhysicalPosition { x, y }, .. } => {
                        if mouse_pressed {
                            let (last_x, last_y) = last_mouse_pos;
                            let scale_factor = (config.height as f64 - last_y) / (config.height as f64 - y).max(0.1);
                            scale *= scale_factor;
                            origin += scale * (last_x - x) / config.width as f64 - scale * (scale_factor - 1.0) * ((x / config.width as f64) - 0.5);
                            window.request_redraw();
                        }
                        last_mouse_pos = (x, y);
                    },
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    },
                    _ => {},
                }
            }
            Event::RedrawRequested(_) => {
                let frame = surface
                    .get_current_texture()
                    .expect("Failed to acquire next swap chain texture");
                let view = frame
                    .texture
                    .create_view(&wgpu::TextureViewDescriptor::default());
                let mut encoder =
                    device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
                let options_bind_group = device.create_bind_group(
                    &wgpu::BindGroupDescriptor {
                        label: Some("options_bind_group"),
                        layout: &options_bind_group_layout,
                        entries: &[wgpu::BindGroupEntry {
                            binding: 0,
                            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                                buffer: &options_buffer,
                                offset: 0,
                                size: None,
                            }),
                        }],
                    });
                {
                    let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                        label: None,
                        color_attachments: &[wgpu::RenderPassColorAttachment {
                            view: &view,
                            resolve_target: None,
                            ops: wgpu::Operations {
                                load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                                store: true,
                            },
                        }],
                        depth_stencil_attachment: None,
                    });
                    rpass.set_bind_group(0, &options_bind_group, &[]);
                    rpass.set_pipeline(&render_pipeline);
                    rpass.draw(0..3, 0..1);
                }

                queue.write_buffer(&options_buffer, 0, & unsafe {
                    mem::transmute::<[f32; 4], [u8; 16]>([config.width as f32, config.height as f32, scale as f32, origin as f32])
                });
                queue.submit(Some(encoder.finish()));
                frame.present();
            }
            _ => {}
        }
    });
}

#[cfg(not(target_arch = "wasm32"))]
pub fn main_native() {
    let event_loop = EventLoop::new();
    let window = winit::window::Window::new(&event_loop).unwrap();
    env_logger::init();
    // Temporarily avoid srgb formats for the swapchain on the web
    pollster::block_on(run(event_loop, window));
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn main_web() {
    let event_loop = EventLoop::new();
    let window = winit::window::Window::new(&event_loop).unwrap();
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("could not initialize logger");
    use winit::platform::web::WindowExtWebSys;
    // On wasm, append the canvas to the document body
    web_sys::window()
        .and_then(|win| win.document())
        .and_then(|doc| doc.body())
        .and_then(|body| {
            body.append_child(&web_sys::Element::from(window.canvas()))
                .ok()
        })
    .expect("couldn't append canvas to document body");
    let client_window = web_sys::window().unwrap();
    window.set_inner_size(
        winit::dpi::LogicalSize::new(
            client_window.inner_width().unwrap().as_f64().unwrap(),
            client_window.inner_height().unwrap().as_f64().unwrap(),
            )
        );
    wasm_bindgen_futures::spawn_local(run(event_loop, window));
}
