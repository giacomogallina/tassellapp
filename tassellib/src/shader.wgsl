[[stage(vertex)]]
fn vs_main([[builtin(vertex_index)]] in_vertex_index: u32) -> [[builtin(position)]] vec4<f32> {
    let x = 2.0 * f32(i32(in_vertex_index) - 1);
    let y = f32(i32(in_vertex_index & 1u) * 4 - 1);
    return vec4<f32>(x, y, 0.0, 1.0);
}

struct Options {
	w: f32;
	h: f32;
	scale: f32;
	origin: f32;
};

[[binding(0), group(0)]]
var<uniform> options: Options;

[[stage(fragment)]]
fn fs_main([[builtin(position)]] pos: vec4<f32>) -> [[location(0)]] vec4<f32> {
	var x: f32 = f32(pos[0] / options.w) * options.scale - 0.5*options.scale + options.origin;
	var y: f32 = f32(options.h - pos[1]) / options.w * options.scale;
	var res: i32 = i32(-round(x));
	x = x - round(x);
	var r2: f32 = x*x + y*y;
	var count: i32 = 0;
	loop {
		if (count >= 20 || r2 >= 1.0) {
			break;
		}
		x = -x / r2;
		y = y / r2;
		res = res - i32(round(x));
		x = x - round(x);
		r2 = x*x + y*y;
		count = count + 1;
	}
	let s = (res + 3*count);
	var sm6 = s - 6 * (s / 6);
	if (sm6 < 0) {
		sm6 = sm6 + 6;
	}
	switch (sm6) {
		case 0: {return vec4<f32>(1.0, 0.1156, 0.0103, 1.0);}
		case 1: {return vec4<f32>(1.0, 1.0, 0.0, 1.0);}
		case 2: {return vec4<f32>(0.1156, 1.0, 0.0103, 1.0);}
		case 3: {return vec4<f32>(0.0, 0.531, 1.0, 1.0);}
		case 4: {return vec4<f32>(0.1156, 0.218, 1.0, 1.0);}
		case 5: {return vec4<f32>(0.531, 0.1156, 1.0, 1.0);}
		default: {return vec4<f32>(0.0, 0.0, 0.0, 1.0);}
	}
}
